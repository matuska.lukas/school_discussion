<?php
    require './config/config.php';
    $mysqli = new mysqli($db_host, $db_user, $db_pass, $db_db);

    // Check connection
    if ($mysqli->connect_error) {
        die("<p>Connect error: " . $mysqli->connect_error . "</p>");
    }

    // Set charset
    $mysqli->set_charset('utf8');
?>
<!DOCTYPE html>
<html lang="cs">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Anonymní diskuze</title>
	
	<!-- Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700&amp;subset=latin-ext" rel="stylesheet">

	<!-- Stylesheets -->
	<link rel="stylesheet" type="text/css" href="/css/style.css">
	<link rel="stylesheet" type="text/css" href="/css/avatars.css">
</head>
<body>
	<header id="main-header">
		<div class="container">
			<div id="logo">
				<img src="/images/anonymous.png" alt="Anonymní diskuze - logo">
			</div>
			<h1>Anonymní diskuze</h1>
		</div>
	</header>
	<div class="container">
		<!-- COMMENT FORM -->
		<form method="POST">
			<!-- AVATAR SELECTION -->
			<div class="avatars">
				<?php $avatar = rand(1, 4); ?>
				<h2>Vyberte si svého avatara:</h2>
				<label class="avatar">
					<input type="radio" name="avatar" value="1" <?php if($avatar == 1){echo "checked";} ?> />
					<img src="/images/avatars/anonymous-1.png" alt="avatar"/>
				</label>
				<label class="avatar">
					<input type="radio" name="avatar" value="2" <?php if($avatar == 2){echo "checked";} ?> />
					<img src="/images/avatars/anonymous-2.png" alt="avatar"/>
				</label>
	
				<label class="avatar">
					<input type="radio" name="avatar" value="3" <?php if($avatar == 3){echo "checked";} ?> />
					<img src="/images/avatars/anonymous-3.png" alt="avatar"/>
				</label>
	
				<label class="avatar">
					<input type="radio" name="avatar" value="4" <?php if($avatar == 4){echo "checked";} ?> />
					<img src="/images/avatars/anonymous-4.png" alt="avatar"/>
				</label>
			</div>
			<!-- MESSAGE INPUT -->
			<div class="message">
				<input name="message" type="text" placeholder="Sem napište anonymní zprávu ..." required>
				<input type="submit" value="Odeslat">
			</div>
		</form>
		<?php
			/* INSERT MESSAGE TO DATABASE */
			if(isset($_POST["message"]) and isset($_POST["avatar"])){
				$message = $mysqli->real_escape_string($_POST["message"]); // SQL INJECTION - MESSAGE
				$avatar = $mysqli->real_escape_string($_POST["avatar"]);  //  SQL INJECTION - AVATAR
				$sql = "INSERT INTO v3prog_comments (content, avatar) VALUES ('$message', '$avatar');";

				/* FEEDBACK FOR USER - PRINT STATUS OF INSERTION TO DB */
				if ($mysqli->query($sql)) {
					?>
					<div class='comment'>
						<p class="messageStatus">Váš příspěvek byl přidán do diskuze.</p>
					</div>
					<?php
				}else{
					echo "<p>Něco se pokazilo! Chyba: <b>" . $mysqli->error . "</b>";
				}
			}

			/* PRINT ALL COMMENTS */
			$sql = "SELECT v3prog_comments.* FROM v3prog_comments ORDER BY v3prog_comments.id DESC;";

			if ($results = $mysqli->query($sql)) {
				while ($row = $results->fetch_array()) {
					?>
					<div class='comment'>
						<img src='/images/avatars/anonymous-<?php echo $row["avatar"];?>.png' alt='Anonymní diskutující' class='avatar'>
						<div class="bubble">
							<div class="date">
								<?php echo $mysqli->real_escape_string($row["date"]); ?>
							</div>
							<div class="content">
								<?php echo $mysqli->real_escape_string($row["content"]); ?>
							</div> 
			</div>
		</div>
		<?php
				}
			}
		?>
	</div>
</body>
</html>